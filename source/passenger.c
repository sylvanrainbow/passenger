#include <stdio.h>
#include <errno.h>


/*#################################################################################################
        EDIT THIS 
#################################################################################################*/
#define BASE_CYPHER         ";4Vz9+vA8&$Xz.tQ7"                  
                                                        // the core set of symbols which is used
                                                        //   as a base for generating the passcode
                                                        //   and defining its length
                                                        // (output will have same amount of chars)
                                                        // WARNING: THIS VALUE MUST BE ASCII, AND
                                                        //   THE VALUE OF EVERY CHAR >31 && <127
                                                        

#define PASSWORD            "somethingmemorable"                              
                                                        // default password used if the user never
                                                        //   generated the password file 

#define ENCRYPTION_LOOPS    7                          
                                                        // Number of encryption applications
                                                        // Change this if you are getting too many
                                                        //   of the same set of symbols (*0Aa)
/*#################################################################################################
    WARNING:    in order for the loops to be effective, use a wide variety of symbols and make sure
                adjacent symbol's value decades do not add up to common numbers (if the base cypher 
                is 16 chars long, and each char in the password starts with a 4, then only 4 values
                in the passcode will actually be different from the base cypher because the loop
                will only ever hit those values)
#################################################################################################*/


#define PROG_NAME		    "passenger"                 // name of this program
#define PASSWORD_MAX        (100 + 1)                   // maximum length of the user-defined pass
                                                        // +1 is for the NUL character
#define PASSWORD_FILE       "passfile.bin"              // path to the file where the user stores 
                                                        //   the user-defined password



//		STRINGLEN
//  Returns number of chars in string ("abcd" = 4).

int stringlen(char * str) { 
	int len = 0;
	while(*str != '\0') {
		str++;
		len++;
	}
	return len;
}


//		STR_COMPARE
//  Compares strings and returns 1 if they are equal. It first compares lengths as a shortcut.

int str_compare(char * a, char * b) {
    int b_length = stringlen(b);
    int a_length = stringlen(a);
    if (b_length != a_length) {
        return 0;
    }
    for (int i = 0; i < b_length; i++) {
        if (b[i] != a[i]) {
            return 0;
        }
    }
    return 1;
}


//      STR_COPY
//  Copies contents of one string to another. It is assumed a size check was performed beforehand
//  to make sure they fit.

void str_copy(char *source, char *destination) {
    while (*source != '\0') {
        *destination = *source;
		source++;
		destination++;
	}
    *destination = '\0';
}


//		PRINT_INFO
//  Prints info about the program.

void print_info(void) {
    printf(
        "\n"
        "################################################################################\n"
        "                               PASSENGER INFO\n"
        "################################################################################\n"
        "\n"
        "ABOUT:\n"
        "  %s generates a passcode with two modifier strings:\n"
        "  - a default password, or a user-generated one which is encrypted and stored\n"
        "  - a common word used as input to consistently generate a passcode\n"
        "  The user passes a known word (like a website name), and a (hopefully)\n"
        "  hardly reversible passcode gets generated which can then be used for a login.\n"
        "  If %s exists, hashed password stored there will be used, unless the\n"
        "  command \"defpass\" is passed to the program to force the default password.\n"
        "\n"
        "USAGE:\n"
        "  [%s], [%s info], [%s help] -> shows this page\n"
        "  [%s setpass my_password] -> stores my_password in %s\n"
        "  [%s website_name]        -> generates consistent passcode\n"
        "  [%s defpass web_name]    -> forces use of default password\n"
        "\n\n"
        "  WARNING: DO NOT USE THIS SOFTWARE BECAUSE IT IS BAD AT WHAT IT DOES\n"
        "\n",
        PROG_NAME, PASSWORD_FILE, PROG_NAME, PROG_NAME, PROG_NAME, PROG_NAME, PASSWORD_FILE,
        PROG_NAME, PROG_NAME
    );
}


//      ROT ENCRYPT
//  Loops through a cypher string and increments each char of the target string by the cypher char
//  value. A new char is generated from this and the initial string is "encrypted".
//  Function returns 1 on success, and 0 on failure (if some char is not ASCII).
//  WARNING: The encrypted value possibly contains weird characters, and should not be edited.

int rot_encrypt(char *str, char *cypher) {
    int pos = -1;                                       // set to -1 so it start with 0 when +1
    int cypher_length = stringlen(cypher);              // get length so we can loop it if needed
    int new_char;                                      // holds the new char 

    while (*str != '\0') {                              // loop through str until we get to NUL
        if (pos < cypher_length) {                      // loop cypher indexes
           pos += 1; 
        }
        else {
            pos = 0;
        }

        if (*str > 126 || *str < 32) {                   // check if char is ASCII
            printf(
                "(;^_^) Oopsie! Password contains char \"%c\", whose value is not in ASCII\n"
                "range 32-126. Please use regular ol' ASCII symbols for your password.\n",
                *str
                );

            return 0;                                   // return failure code if error encountered
        }

        new_char = (*str - 32) + (cypher[pos] - 32);    // shift the pass number by cypher value
                                                        // subtract 32 to make the lowest valid
                                                        // value (32) into 0

        if (new_char > (126 - 32)) {                    // if the value exceeds (126 - 32) 
            new_char = new_char - (126 - 32);           //   (normalized value), make it less
        }
        new_char += 32;                                 // add the 32 back to get the actual ascii
                                                        //   value which should now be in range

        *str = (char) new_char;                         // replace initial char with the new one

		str++;                                          // increment pointer to next char
	}

    return 1;                                           // return success code
}


//      ROT DECRYPT
//  Reverses the encryption of rot_encrypt()

int rot_decrypt(char *str, char *cypher) {
    int pos = -1;                                       // set to -1 so it start with 0 when +1
    int cypher_length = stringlen(cypher);              // get length so we can loop it if needed
    int new_char;                                      // holds the new char 

    while (*str != '\0') {                              // loop through str until we get to NUL
        if (pos < cypher_length) {                      // loop cypher indexes
           pos += 1; 
        }
        else {
            pos = 0;
        }

        if (*str > 126 || *str < 32) {                   // check if char is ASCII
            printf(
                "(;^_^) Oopsie! Encrypted password contains char \"%c\", whose value is not in\n"
                "ASCII range 32-126. This should never happen. Please use the built-in\n"
                "\"setpass\" command to generate a valid encrypted password.\n",
                *str
                );

            return 0;                                   // return failure code if error encountered
        }

        new_char = (*str - 32) - (cypher[pos] - 32);    // shift the pass number by cypher value
                                                        // subtract 32 to make the lowest valid
                                                        // value (32) into 0

        if (new_char < 0) {                             // if the value lower than 0
            new_char = new_char + (126 - 32);           // make it valid
        }
        new_char += 32;                                 // add the 32 back to get the actual ascii
                                                        //   value which should now be in range

        *str = (char) new_char;                         // replace initial char with the decrypted

		str++;                                          // increment pointer to next char
	}

    *str = '\0';

    return 1;                                           // return success code
}


//      FILE TO STR
//  Reads chars into char array until EOF or limit. Appends a NUL, so max char limit is one less.
//  Returns 1 on success, and 0 on failure.

int file_to_str(FILE *stream, char *str, int max_size) {
    *str = fgetc(stream);                               // copy initial character
    max_size -= 1;                                      // indicate 1 letter copied
    while (*str != EOF) {                               // test for End Of File
        str++;                                          // move pointer to next char
        *str = fgetc(stream);                           // copy another char from the file
        max_size -= 1;                                  // indicate char copied
        if (max_size < 1) {                             // if max size is 0, abort everything
            printf(
                "(;^_^) Oopsie! The encrypted password length from file %s\n"
                "exceeds max limit of %d chars. Please use the built-in \"setpass\"\n"
                "command to generate a valid encrypted password.\n",
                PASSWORD_FILE, (PASSWORD_MAX - 1)
            );
            return 0;
        }
    }
    *str = '\0';                                        // when the current char is EOF, the 
                                                        //   pointer won't get advanced, and the
                                                        //   char will get replaced with '\0'
    return 1;
}


//      GENERATE PASSCODE
//  Changes the characters in the passcode string by alternatingly applying modifier strings to it.
//  Each char in a modifier gets separated into two binary values. The left digit(s) determine the
//  passcode index, and the right digit changes the value of the passcode char pointed to.
//  EDITING: modifier processing has been separated so math can be adjusted for more variation.
//  WARNING: it could happen that the position skips align in such a way that not many chars get
//  encrypted. Use a weird base cypher length to ensure the loops hit every character or change
//  the math to skip a different number, go letter by letter, or inverse when it wants to overflow.

void generate_passcode(char *ppasscode, char *first_modifier, char *second_modifier, int loops) {
    int passcode_length = stringlen(ppasscode);
    int passcode_pos = 0;
    char *modifier_ptr;                                 // holds pointer to 1st or 2nd modifier
    int move_by;
    int change_by;
    int current_char_value;                             // used for math because of signed chars
                                                        // which can't fit >126 positive values

    for (; loops > 0; loops--) {                        // loops is already initialized as a local
                                                        //   var, so we don't need the first part

        // FIRST MODIFIER (edit numbers if necessary)
        modifier_ptr = first_modifier;                  // copy the first mod ptr so we can loop
                                                        //   it without losing it

        while (*modifier_ptr != '\0') {                 // loop until NUL char
            move_by = (*modifier_ptr / 10);             // left digit(s) of a number determine pos
            change_by = (*modifier_ptr % 10);           // right digit determines the shift amount

            passcode_pos += move_by;                    // set new pos
            while (passcode_pos >= passcode_length) {   // if pos out of bounds, reel it back
                passcode_pos -= passcode_length;        //   as many times as needed (1 usually)
            }

            current_char_value = ppasscode[passcode_pos];
            current_char_value += change_by;            // change value of char
            if (current_char_value > 126) {             // if value exceeds ASCII 126
                current_char_value -= (126 - 32);       // make it so it overflows to 33 
                                                        // (space (32) is not used)
            }
            ppasscode[passcode_pos] = current_char_value;

            modifier_ptr++;                           // advance pointer position
        }

        // SECOND MODIFIER (edit numbers if necessary)
        modifier_ptr = second_modifier;                 // copy the second mod ptr so we can loop
                                                        //   it without losing it

        while (*modifier_ptr != '\0') {              // loop until NUL char
            move_by = (*modifier_ptr / 10);          // left digit(s) of a number determine pos
            change_by = (*modifier_ptr % 10);        // right digit determines the shift amount

            passcode_pos += move_by;                    // set new pos
            while (passcode_pos >= passcode_length) {   // if pos out of bounds, reel it back
                passcode_pos -= passcode_length;        //   as many times as needed (1 usually)
            }

            current_char_value = ppasscode[passcode_pos];
            current_char_value += change_by;            // change value of char at specified index
            if (current_char_value > 126) {             // if value exceeds ASCII 126
                current_char_value -= (126 - 32);       // make it so it overflows to 33 
                                                        // (space (32) is not used)
            }
            ppasscode[passcode_pos] = current_char_value;

            modifier_ptr++;                          // advance pointer position
        }
    }
}


int main(int argc, char *argv[]) {

    char passcode[]             = BASE_CYPHER;          // make char array out of base cypher
    char password[PASSWORD_MAX] = PASSWORD;             // make char array out of password, but
                                                        //   it can be overwritten if using custom
                                                        //   because it is very a very big array



    if (argc == 1) {                                    // if only program name
        print_info();                                   // show the info page
    }
    else if (argc == 2) {                               // if program name and 1 argument
        if (str_compare(argv[1], "info") || str_compare(argv[1], "help")) {
            print_info();                               // show info page if requested
        }
        else {                                          // treat arg as input for pass generation
            FILE *fp;                                   // declare file pointer

            fp = fopen(PASSWORD_FILE, "rb");            // try to read file
            if (fp == NULL) {                           // if error while opening (errno is set)
                if (errno == ENOENT) {                  // ENOENT == 2 == "no such file or dir"
                                                        // if no file, default password is used
                    generate_passcode(passcode, argv[1], password, ENCRYPTION_LOOPS);
                    printf(
                        "Passcode generated with default password:\n%s\n",
                        passcode
                    );
                }
                else {                                  // if error unrelated to ENOENT
                    perror("(;^_^) Oopsie! ERROR");     // errno message gets printed
                }
            }
            else {                                      // if there is a file, the password gets
                                                        //   unencrypted and used
                if (file_to_str(fp, password, PASSWORD_MAX)) {
                    if (rot_decrypt(password, passcode)) {
                        generate_passcode(passcode, argv[1], password, ENCRYPTION_LOOPS);
                        printf(
                            "Passcode generated with password from file %s:\n%s\n",
                            PASSWORD_FILE, passcode
                        );
                    }
                }

                fclose(fp);                             // close file
            }
        }
    }
    else if (argc == 3) {
        if (str_compare(argv[1], "setpass")) {          // "Set Password" is used to set up
                                                        //   a file with an encrypted password
                                                        //   which is user-defined and not default

            if (stringlen(argv[2]) < PASSWORD_MAX) {    // if proposed password is of fitting size
                str_copy(argv[2], password);            // copy argv to the password char array

                if (rot_encrypt(password, passcode)) {  // if encryption of password was successful
                    FILE *fp;                           // declare file pointer

                    fp = fopen(PASSWORD_FILE, "w");     // open file for writing. if no file create
                    if (fp == NULL) {                   // if opening file failed
                        perror("(;^_^) Oopsie! ERROR"); // errno message gets printed
                    }
                    else {                              // if file succesfully opened for writing
                        fputs(password, fp);            // write encrypted password to the file    
                        printf(
                            "Password has been encrypted and stored in %s\n"
                            "This password will now be used for encryption unless instructed otherwise.\n",
                            PASSWORD_FILE 
                        );        
                        fclose(fp);                     // close file
                    }
                }
            }
            else {                                      // if proposed pass too long, give error
                printf(
                    "(;^_^) Oopsie! The proposed password length exceeds max limit of %d\n",
                    (PASSWORD_MAX - 1)
                );
            }
        }
        else if (str_compare(argv[1], "defpass")) {     // "Default Password" is used to force
                                                        //   the usage of the default password
                                                        //   in a scenario where a user-defined
                                                        //   password exists in its own file
            generate_passcode(passcode, argv[2], password, ENCRYPTION_LOOPS);
            printf(
                "Passcode generated with default password:\n%s\n",
                passcode
            );
        }
        else {
            puts(
                "(;^_^) Oopsie! 2 arguments passed to program, but the first command is not\n"
                "recognized. Please use the \"info\" or \"help\" commands to learn more.\n"
            );
        }
    }
    else {
        puts("(;^_^) Oopsie! Too many argumentspassed to program! Expected 2 or less.\n");
    }

    return 0;                                           // close the program naturally
}
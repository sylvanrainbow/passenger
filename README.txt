WARNING: 
  DO NOT USE THIS SOFTWARE BECAUSE IT IS BAD AT WHAT IT DOES

  Copying passwords is dangerous because they may get stored in your clipboard history.
  This software does not handle clipboards. You would have to erase history yourself.

IN SHORT:
  Passenger generates a hash. Consistently, given the same argument.
  From a piece of info the user has, and a piece he always knows.
                                   _____________
  - Info user knows by heart ----> |           |
      (e.g. a password)            | PASSENGER | ---> Hash 
  - Info user can deduct --------> |___________|      (e.g. a password for a specific website
      (e.g. a website name)                           generated from a known common password)
  
ABOUT:
  In the binary, there is a secret cypher, and a "password" (which should be edited before 
  compiling). The cypher determines the length of the output hash, and is the base onto which
  the other modifiers get applied. The "password" is a string stored in the binary, but it can
  also be stored in an outside file caled passfile.bin. The password is encrypted when stored
  to the file. If the file exists, that password will be decrypted and used unless the command
  "defpass" is invoked (in which case, the default password inside the binary will be used).
  The other modifier is an argument the user passes to the program.

  The initial idea started as a joke about using the same password everywhere. The "password"
  part is intended to be this memorable password that the user knows and uses everywhere. Using
  the cypher, this password could be turned into a consistent static output. But that would be
  only slightly safer than using the other same password everywhere. For that problem, the 
  solution was to be a variable argument that the user could pass to the program to modify the
  other combination and so generate different outputs. But how to remember the arguments? Easy.
  The only thing one would have to do is:
    1. store the binary or the base_cypher in a safe place so they don't get lost
    2. remember the password one already knows
    3. use the name of the website one is generating the login for - as the argument
  That way, whether one stored the password in the binary or outside, every time one would want
  to re-generate a password for a website, one would just pass the website name to the program.

  For extra security, a passphrase could be used. 

  It is possible to generate a hash that doesn't meet the requirement for 1 number, 1 normal
  letter, 1 capital letter and 1 symbol, that some websites have. One cannot prepare in 
  advance for every scenario, but changing any of the 3 editable variables in the source code
  should be enough to get a combination which would generate the desired hash type for all
  inputs you wish to test out.

USAGE:
  [passenger]                       -> shows info page
  [passenger info]                  -> shows info page
  [passenger help]                  -> shows info page
  [passenger setpass my_password]   -> stores my_password in passfile.bin
  [passenger website_name]          -> generates consistent passcode
  [passenger defpass website_name]  -> forces use of default password

